$(document).ready(function() 
{
    /*$('#vertical').lightSlider({
      gallery:true,
      item:1,
      vertical:true,
      verticalHeight:417,
      vThumbWidth:100,
      thumbItem:8,
      thumbMargin:0,
      slideMargin:0
    });

    //mobile
    $('#imageGallery').lightSlider({
       adaptiveHeight:false,
        item:1,
        slideMargin:0,
        loop:true,
        verticalHeight:300,
        
    });   */

  var smaGallery = 
  {
    wrapper: null,
    main: null,
    nav: null,
    thumb: null,
    count: 300,
    index: 1,

    //
    //Init gallery
    //
    init: function( wrapper )
    {
      this.wrapper = $( '#desktop .gallery-wrap' );
      this.main    = $( '.gallery-main', this.wrapper );
      this.nav     = $( '.gallery-nav', this.wrapper );
      this.thumb   = $( '.gallery-thumb', this.wrapper );

      this.buildElements();
      this.initNav();
      this.initThumbs();
    },

    //
    //Build gallery element
    buildElements: function()
    {
      //Build thumbnail
      var ul    = $( 'ul', this.thumb );
      var count = $( 'li', ul ).length;

      for ( var i = (count+1); i < (this.count + 1);i++ )
      {
        var item = '<li data-image="assets/images/event/img-' + i + '.jpg" data-index="'+i+'" class="no-load" style="display:none;"></li>';

        ul.append( item );
      }

      //Build large
      var items = $( '.gallery-items', this.main );
      var count = $( '.gallery-item', items ).length;

      for ( var i = (count+1); i < (this.count + 1);i++ )
      {
        var item = '<div class="gallery-item no-load" data-index="'+i+'" style="display:none;"></div>';

        items.append( item );
      }
    },

    //
    //Init navigation
    initNav: function()
    {
      var $this = this;

      //Prev
      $( '.gallery-nav-prev', $this.nav ).click( function()
      {
        var prev = $this.index - 1;
        if ( prev < 1 ) {
          return false;
        }
        $this.toSlide( prev );
        return false;
      });

      //Next
      $( '.gallery-nav-next', $this.nav ).click( function()
      {
        var next = $this.index + 1;
        if ( next > $this.count ) {
          return false;
        }
        $this.toSlide( next );
        return false;
      });
    },

    //
    //Init thumbs
    initThumbs: function()
    {
      var $this = this;

      $( 'li', $this.thumb ).each( function()
      {
        $this.initThumb( $(this) );
      });
    },

    //
    //Init thumb
    initThumb: function( item )
    {
      var $this = this;

      item.click( function()
      {
        var index = $(this).data( 'index' );
        if ( index < 1 || index > $this.count ) {
          return false;
        }
        $this.toSlide( index );
        return false;
      });
    },

    //
    //Move thumb up
    moveThumbUp: function( showIndex, index )
    {
      var $this  = this;
      var thumb1 = $( 'li[data-show="1"]', $this.thumb ).addClass( 'up' );
      var thumb2 = $( 'li[data-show="2"]', $this.thumb ).addClass( 'up' );
      var thumb3 = $( 'li[data-show="3"]', $this.thumb ).addClass( 'up' );
      var thumb4 = $( 'li[data-show="4"]', $this.thumb ).addClass( 'up' );
      var thumb5 = $( 'li[data-show="5"]', $this.thumb ).addClass( 'up' );

      var index5 = thumb5.data( 'index' );
      var thumb6 = $( 'li[data-index="' + (index5+1) + '"]', $this.thumb );
      thumb6.addClass( 'image6' ).show();
      thumb6.addClass( 'up' );

      setTimeout( function()
      {
        thumb1.removeAttr( 'data-show' );
        thumb1.removeClass( 'image1' );
        thumb1.removeClass( 'up' );
        thumb1.addClass( 'image0' );

        thumb2.attr( 'data-show', 1 );
        thumb2.removeClass( 'image2' );
        thumb2.removeClass( 'up' );
        thumb2.addClass( 'image1' );

        thumb3.attr( 'data-show', 2 );
        thumb3.removeClass( 'image3' );
        thumb3.removeClass( 'up' );
        thumb3.addClass( 'image2' );

        thumb4.attr( 'data-show', 3 );
        thumb4.removeClass( 'image4' );
        thumb4.removeClass( 'up' );
        thumb4.addClass( 'image3' );

        thumb5.attr( 'data-show', 4 );
        thumb5.removeClass( 'image5' );
        thumb5.removeClass( 'up' );
        thumb5.addClass( 'image4' );

        thumb6.attr( 'data-show', 5 );
        thumb6.removeClass( 'image6' );
        thumb6.removeClass( 'up' );
        thumb6.addClass( 'image5' );
      }, 
      500 );
    },

    //
    //Move thumb down
    moveThumbDown: function( showIndex, index )
    {
      var $this  = this;
      var thumb1 = $( 'li[data-show="1"]', $this.thumb ).addClass( 'down' );
      var thumb2 = $( 'li[data-show="2"]', $this.thumb ).addClass( 'down' );
      var thumb3 = $( 'li[data-show="3"]', $this.thumb ).addClass( 'down' );
      var thumb4 = $( 'li[data-show="4"]', $this.thumb ).addClass( 'down' );
      var thumb5 = $( 'li[data-show="5"]', $this.thumb ).addClass( 'down' );

      console.log( thumb1 );

      var index1 = thumb1.attr( 'data-index' );
      var thumb0 = $( 'li[data-index="' + (index1-1) + '"]', $this.thumb );
      thumb0.addClass( 'image0' ).show();
      thumb0.addClass( 'down' );

      setTimeout( function()
      {
        thumb0.attr( 'data-show', 1 );
        thumb0.removeClass( 'image0' );
        thumb0.removeClass( 'down' );
        thumb0.addClass( 'image1' );

        thumb1.attr( 'data-show', 2 );
        thumb1.removeClass( 'image1' );
        thumb1.removeClass( 'down' );
        thumb1.addClass( 'image2' );

        thumb2.attr( 'data-show', 3 );
        thumb2.removeClass( 'image2' );
        thumb2.removeClass( 'down' );
        thumb2.addClass( 'image3' );

        thumb3.attr( 'data-show', 4 );
        thumb3.removeClass( 'image3' );
        thumb3.removeClass( 'down' );
        thumb3.addClass( 'image4' );

        thumb4.attr( 'data-show', 5 );
        thumb4.removeClass( 'image4' );
        thumb4.removeClass( 'down' );
        thumb4.addClass( 'image5' );

        thumb5.removeAttr( 'data-show' );
        thumb5.removeClass( 'image5' );
        thumb5.removeClass( 'down' );
        thumb5.addClass( 'image6' );
      }, 
      500 );
    },

    //
    //toSlide
    toSlide: function( index )
    {
      //Clear slides
      this.clearSlides( index );

      var $this = this;
      var thumb = $( 'li[data-index="' + index + '"]', $this.thumb );
      if ( thumb.length < 1 ) {
        return false;
      }

      //
      //Move thumb
      //
      var showIndex = thumb.attr( 'data-show' );
      thumb.addClass( 'active' );

      if ( showIndex == 1 && index > 1 )
      {
        $this.moveThumbDown( showIndex, index );
      }
      else if ( showIndex == 5 )
      {
        $this.moveThumbUp( showIndex, index );
      }

      //
      //Change image
      //
      $this.changeImage( index );

      //
      //Prepare move image
      $this.prepareMoveImage( index );
      $this.prepareMoveThumb( index );

      $this.index = index;
    },

    //
    //Change image
    changeImage: function( index )
    {
      var $this = this;
      var imageHeight = $this.main.height();
      var goTop = imageHeight * (index-1);

      $( '.gallery-items', $this.main).velocity({
        'translateY': '-'+goTop+'px'
      }, 600 );
    },

    //
    //PrepareMoveImage
    prepareMoveImage: function( index )
    {
      var $this = this;

      //Prepare next index
      for ( var i = 1; i < 4; i++ )
      {
        var nextIndex = index + i;
        if ( nextIndex <= $this.count ) 
        {
          var nextImage = $( '.gallery-item[data-index="'+nextIndex+'"]', $this.main );
          if ( nextImage.length > 0 && nextImage.hasClass( 'no-load' ) )
          {
            var nextThumb = $( 'li[data-index="'+nextIndex+'"]', $this.thumb );
            nextImage.append( '<img src="' + nextThumb.data( 'image' ) + '">' );
            nextImage.removeClass( 'no-load' );
            nextImage.show();
          }
        }
      }
    },

    //
    //PrepareMoveThumb
    prepareMoveThumb: function( index )
    {
      var $this = this;

      //Prepare next index
      for ( var i = 1; i < 4; i++ )
      {
        var nextIndex = index + i;
        if ( nextIndex <= $this.count ) 
        {
          var nextImage = $( 'li[data-index="'+nextIndex+'"]', $this.thumb );
          if ( nextImage.length > 0 && nextImage.hasClass( 'no-load' ) )
          {
            nextImage.append( '<img src="' + nextImage.data( 'image' ) + '">' );
            nextImage.removeClass( 'no-load' );
          }
        }
      }
    },

    //
    //Clearing slides
    clearSlides: function( index )
    {
      $( 'li', this.thumb ).removeClass( 'active' );
    }
  };

  smaGallery.init();

  var smaGalleryMobile = 
  {
    wrapper: null,
    main: null,
    nav: null,
    thumb: null,
    count: 300,
    index: 1,

    //
    //Init gallery
    //
    init: function( wrapper )
    {
      this.wrapper = $( '#mobile .gallery-wrap' );
      this.main    = $( '.gallery-main', this.wrapper );
      this.nav     = $( '.gallery-nav', this.wrapper );
      this.thumb   = $( '.gallery-thumb', this.wrapper );

      this.buildElements();
      this.initNav();
      this.initThumbs();
    },

    //
    //Build gallery element
    buildElements: function()
    {
      //Build thumbnail
      var ul    = $( 'ul', this.thumb );
      var count = $( 'li', ul ).length;

      for ( var i = (count+1); i < (this.count + 1);i++ )
      {
        var item = '<li data-image="assets/images/event/img-' + i + '.jpg" data-index="'+i+'" class="no-load" style="display:none;"></li>';

        ul.append( item );
      }

      //Build large
      var items = $( '.gallery-items', this.main );
      var count = $( '.gallery-item', items ).length;

      for ( var i = (count+1); i < (this.count + 1);i++ )
      {
        var item = '<div class="gallery-item no-load" data-index="'+i+'" style="display:none;"></div>';

        items.append( item );
      }
    },

    //
    //Init navigation
    initNav: function()
    {
      var $this = this;

      //Prev
      $( '.gallery-nav-prev', $this.nav ).click( function()
      {
        var prev = $this.index - 1;
        if ( prev < 1 ) {
          return false;
        }
        $this.toSlide( prev );
        return false;
      });

      //Next
      $( '.gallery-nav-next', $this.nav ).click( function()
      {
        var next = $this.index + 1;
        if ( next > $this.count ) {
          return false;
        }
        $this.toSlide( next );
        return false;
      });
    },

    //
    //Init thumbs
    initThumbs: function()
    {
      var $this = this;

      $( 'li', $this.thumb ).each( function()
      {
        $this.initThumb( $(this) );
      });
    },

    //
    //Init thumb
    initThumb: function( item )
    {
      var $this = this;

      item.click( function()
      {
        var index = $(this).data( 'index' );
        if ( index < 1 || index > $this.count ) {
          return false;
        }
        $this.toSlide( index );
        return false;
      });
    },

    //
    //Move thumb up
    moveThumbUp: function( showIndex, index )
    {
      var $this  = this;
      var thumb1 = $( 'li[data-show="1"]', $this.thumb ).addClass( 'up' );
      var thumb2 = $( 'li[data-show="2"]', $this.thumb ).addClass( 'up' );
      var thumb3 = $( 'li[data-show="3"]', $this.thumb ).addClass( 'up' );
      var thumb4 = $( 'li[data-show="4"]', $this.thumb ).addClass( 'up' );
      var thumb5 = $( 'li[data-show="5"]', $this.thumb ).addClass( 'up' );

      var index5 = thumb5.data( 'index' );
      var thumb6 = $( 'li[data-index="' + (index5+1) + '"]', $this.thumb );
      thumb6.addClass( 'image6' ).show();
      thumb6.addClass( 'up' );

      setTimeout( function()
      {
        thumb1.removeAttr( 'data-show' );
        thumb1.removeClass( 'image1' );
        thumb1.removeClass( 'up' );
        thumb1.addClass( 'image0' );

        thumb2.attr( 'data-show', 1 );
        thumb2.removeClass( 'image2' );
        thumb2.removeClass( 'up' );
        thumb2.addClass( 'image1' );

        thumb3.attr( 'data-show', 2 );
        thumb3.removeClass( 'image3' );
        thumb3.removeClass( 'up' );
        thumb3.addClass( 'image2' );

        thumb4.attr( 'data-show', 3 );
        thumb4.removeClass( 'image4' );
        thumb4.removeClass( 'up' );
        thumb4.addClass( 'image3' );

        thumb5.attr( 'data-show', 4 );
        thumb5.removeClass( 'image5' );
        thumb5.removeClass( 'up' );
        thumb5.addClass( 'image4' );

        thumb6.attr( 'data-show', 5 );
        thumb6.removeClass( 'image6' );
        thumb6.removeClass( 'up' );
        thumb6.addClass( 'image5' );
      }, 
      500 );
    },

    //
    //Move thumb down
    moveThumbDown: function( showIndex, index )
    {
      var $this  = this;
      var thumb1 = $( 'li[data-show="1"]', $this.thumb ).addClass( 'down' );
      var thumb2 = $( 'li[data-show="2"]', $this.thumb ).addClass( 'down' );
      var thumb3 = $( 'li[data-show="3"]', $this.thumb ).addClass( 'down' );
      var thumb4 = $( 'li[data-show="4"]', $this.thumb ).addClass( 'down' );
      var thumb5 = $( 'li[data-show="5"]', $this.thumb ).addClass( 'down' );

      console.log( thumb1 );

      var index1 = thumb1.attr( 'data-index' );
      var thumb0 = $( 'li[data-index="' + (index1-1) + '"]', $this.thumb );
      thumb0.addClass( 'image0' ).show();
      thumb0.addClass( 'down' );

      setTimeout( function()
      {
        thumb0.attr( 'data-show', 1 );
        thumb0.removeClass( 'image0' );
        thumb0.removeClass( 'down' );
        thumb0.addClass( 'image1' );

        thumb1.attr( 'data-show', 2 );
        thumb1.removeClass( 'image1' );
        thumb1.removeClass( 'down' );
        thumb1.addClass( 'image2' );

        thumb2.attr( 'data-show', 3 );
        thumb2.removeClass( 'image2' );
        thumb2.removeClass( 'down' );
        thumb2.addClass( 'image3' );

        thumb3.attr( 'data-show', 4 );
        thumb3.removeClass( 'image3' );
        thumb3.removeClass( 'down' );
        thumb3.addClass( 'image4' );

        thumb4.attr( 'data-show', 5 );
        thumb4.removeClass( 'image4' );
        thumb4.removeClass( 'down' );
        thumb4.addClass( 'image5' );

        thumb5.removeAttr( 'data-show' );
        thumb5.removeClass( 'image5' );
        thumb5.removeClass( 'down' );
        thumb5.addClass( 'image6' );
      }, 
      500 );
    },

    //
    //toSlide
    toSlide: function( index )
    {
      //Clear slides
      this.clearSlides( index );

      var $this = this;
      var thumb = $( 'li[data-index="' + index + '"]', $this.thumb );
      if ( thumb.length < 1 ) {
        return false;
      }

      //
      //Move thumb
      //
      var showIndex = thumb.attr( 'data-show' );
      thumb.addClass( 'active' );

      if ( showIndex == 1 && index > 1 )
      {
        $this.moveThumbDown( showIndex, index );
      }
      else if ( showIndex == 5 )
      {
        $this.moveThumbUp( showIndex, index );
      }

      //
      //Change image
      //
      $this.changeImage( index );

      //
      //Prepare move image
      $this.prepareMoveImage( index );
      $this.prepareMoveThumb( index );

      $this.index = index;
    },

    //
    //Change image
    changeImage: function( index )
    {
      var $this = this;
      var imageHeight = $this.main.height();
      var goTop = imageHeight * (index-1);

      $( '.gallery-items', $this.main).velocity({
        'translateY': '-'+goTop+'px'
      }, 600 );
    },

    //
    //PrepareMoveImage
    prepareMoveImage: function( index )
    {
      var $this = this;

      //Prepare next index
      for ( var i = 1; i < 4; i++ )
      {
        var nextIndex = index + i;
        if ( nextIndex <= $this.count ) 
        {
          var nextImage = $( '.gallery-item[data-index="'+nextIndex+'"]', $this.main );
          if ( nextImage.length > 0 && nextImage.hasClass( 'no-load' ) )
          {
            var nextThumb = $( 'li[data-index="'+nextIndex+'"]', $this.thumb );
            nextImage.append( '<img src="' + nextThumb.data( 'image' ) + '">' );
            nextImage.removeClass( 'no-load' );
            nextImage.show();
          }
        }
      }
    },

    //
    //PrepareMoveThumb
    prepareMoveThumb: function( index )
    {
      var $this = this;

      //Prepare next index
      for ( var i = 1; i < 4; i++ )
      {
        var nextIndex = index + i;
        if ( nextIndex <= $this.count ) 
        {
          var nextImage = $( 'li[data-index="'+nextIndex+'"]', $this.thumb );
          if ( nextImage.length > 0 && nextImage.hasClass( 'no-load' ) )
          {
            nextImage.append( '<img src="' + nextImage.data( 'image' ) + '">' );
            nextImage.removeClass( 'no-load' );
          }
        }
      }
    },

    //
    //Clearing slides
    clearSlides: function( index )
    {
      $( 'li', this.thumb ).removeClass( 'active' );
    }
  };

  smaGalleryMobile.init();
 
});

