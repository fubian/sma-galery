// main.js

(function($)
{
	"use strict";

	$(document).ready(function()
	{
		
		$('body').click( function()
		{
			var menu = $( '.menu-fake' );
			var firstText = $( '.text-from-about' );
			var textTop = $('.text-top-from-about');
			var lineHr = $('.black-line');
			var mainPage = $('.fake-index-page');
			var afterPage = $('#about-direct');
			var afterSkew = $('.look-skew');
			menu.addClass( 'line' );

			setTimeout( function()
			{
				menu.addClass( 'line-edit' );
				$( '.menu-fake-inner', menu ).velocity({
					height: '240px',
					duration: 200
				});
			}, 400);

			setTimeout( function()
			{
				menu.addClass( 'menu-edit' );
				$( '#main-index-page .index-fake' ).addClass( 'clear-bg' );
			}, 800);

			setTimeout( function()
			{
				menu.addClass( 'menu-edit2' );
				$( 'li:first-child', menu ).velocity({
					paddingLeft: '60px',
					duration: 200
				});

			}, 1000);

			setTimeout( function()
			{
				menu.addClass( 'menu-edit3' );
				$( 'li:nth-child(3)', menu ).velocity({
					paddingLeft: '60px',
					duration: 200
				});

			}, 1400);

			setTimeout( function()
			{
				menu.addClass( 'menu-edit4' );
				$( 'li:nth-child(4)', menu ).velocity({
					paddingLeft: '90px',
					duration: 200
				});

			}, 1800);

			setTimeout( function()
			{
				menu.addClass( 'menu-edit5' );
				$( 'li:nth-child(5)', menu ).velocity({
					paddingLeft: '20px',
					duration: 200
				});

			}, 2200);

			setTimeout( function()
			{
				menu.addClass( 'menu-edit6' );
				$( 'li:nth-child(6)', menu ).velocity({
					paddingLeft: '30px',
					duration: 200
				});

			}, 2500);

			setTimeout( function()
			{
				$( '.menu-fake-inner', menu ).velocity({
					height: 'auto',
					duration: 200
				});

			}, 2700);

			setTimeout( function()
			{
				menu.removeClass( 'line-edit' );
				menu.removeClass( 'line' );
				menu.addClass( 'clear-line' );

			}, 3700);

			setTimeout( function()
			{
				$( '.menu-toggle' ).velocity({
					right: '50px',
					duration: 200
				});

			}, 3200);

			setTimeout( function()
			{
				$( '.fake-index-page' ).addClass( 'change-bg' );

			}, 2000);

			setTimeout( function()
			{
				$( '.fake-index-page' ).addClass( 'change-bg2' );

			}, 3000);

			setTimeout( function()
			{
				$( '.fake-index-page' ).addClass( 'change-bg3' );

			}, 4200);

			setTimeout( function()
			{
				$( '.logo' ).velocity({
					marginLeft: '0',
					top: '50px',
					left: '40px',
					duration: 200
				});

			}, 4200);

			setTimeout( function()
			{
				menu.addClass( 'menu-right' );
				menu.velocity({
					top: '18px',
					right: '-30px',
					duration: 200
				});

			}, 4700);

			setTimeout( function()
			{
				$( '.menu-toggle' ).velocity({
					color: '#7a3b12',
					duration: 200
				});

			}, 5000);

			setTimeout( function()
			{
				$( 'li:first-child', menu ).addClass( 'show-bg' );

			}, 5400);

			setTimeout( function()
			{
				$( 'li:nth-child(2)', menu ).addClass( 'show-bg' );

			}, 5600);

			setTimeout( function()
			{
				$( 'li:nth-child(3)', menu ).addClass( 'show-bg' );

			}, 5800);

			setTimeout( function()
			{
				$( 'li:nth-child(4)', menu ).addClass( 'show-bg' );

			}, 6000);

			setTimeout( function()
			{
				$( 'li:nth-child(5)', menu ).addClass( 'show-bg' );

			}, 6200);

			setTimeout( function()
			{
				$( 'li:nth-child(6)', menu ).addClass( 'show-bg' );

			}, 6500);

			setTimeout( function()
			{
				var image = $( '.home-fake-image' );
				image.velocity({
					translateY: '100px',
					width: '90%',
					opacity: '0.8',
					duration: 200
				});
				image.velocity({
					translateX: '-70px',
					opacity: '0.4',
					width: '50%',
					duration: 500
				});
				image.velocity({
					translateX: '-30px',
					width: '20%',
					opacity: '0',
					duration: 800
				});

			}, 400);

			setTimeout( function()
			{
				$( '.home-fake-image' ).addClass( 'hidden' );

			}, 1500);

			setTimeout( function()
			{
				
				firstText.velocity({
					scale: 2,
					duration: 1800
				});
				firstText.velocity({
					scale: 1,
					duration: 1000
				});
			}, 600);

			setTimeout( function()
			{
				firstText.velocity({
					translateX: '80%',
					color: '#f50257',
					duration: 1800
				});
				firstText.velocity({
					translateX: '20%',
					color: '#fff',
					duration: 1500
				});
				
			}, 1000);


			setTimeout( function()
			{
				textTop.velocity({
					top: '60%',
					duration: 100
				});
			}, 2500);

			setTimeout( function()
			{
				firstText.velocity({
					translateX: '80%',
					duration: 1800
				});
				
			}, 2800);

			setTimeout( function()
			{
				textTop.velocity({
					top: '80%',
					color: '#fff',
					duration: 200
				});
			}, 3500);

			setTimeout( function()
			{
				firstText.addClass('line');
				firstText.velocity({
					width: '20%',
					opacity: 1
				});
				firstText.velocity({
					width: '50%',
					duration: 1500
				});
				
			}, 3000);


			setTimeout( function()
			{
				lineHr.velocity({
					width: '400px',
					duration: 1800
				});
			}, 4000);

			setTimeout( function()
			{
				firstText.removeClass('line');
				
			}, 4900);


			setTimeout( function()
			{
				var disrupt = $('.disrupt-text');
				disrupt.velocity({
					opacity: '1',
					duration: 1200
				});

			}, 1500);

			setTimeout( function()
			{
				$('.disrupt-text').addClass('skew');
			}, 3000);

			setTimeout( function()
			{
				$('.disrupt-text').addClass('normal');
				$('.disrupt-text').velocity({
					padding:'0',
					width:'0',
					duration: 2000
				});
			}, 3800); 

			

			setTimeout( function()
			{
					mainPage.velocity({
					opacity: 0,
					duration: 200
					
				});
			}, 5600);
			setTimeout(function()
			{
					afterPage.velocity({
					opacity: 1,
					zIndex: '3000',
					duration: 200
				});
			}, 5800);
			


			
			

		});

	});

})(jQuery);